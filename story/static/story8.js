$(document).ready(function() {
    
    $("#search-input").on("keyup", function(e) {
        var q = e.currentTarget.value.toLowerCase()
        console.log(q); //q is the search query

        //check if sidebar is in frame then toggle sidebar out
        if ( !$('#sidebar2').hasClass('active')){
            $('#sidebarCollapse').click();
        }

        $.ajax({
            url: "data?q=" + q,
            success: function (response) {
                $('#search-result').html('') //reset page content every time after search is triggered
                    var result = '';
                    for (var i = 0; i < response.items.length; i++) {
                        item = response.items[i].volumeInfo;

                        result += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                            "<td><img class='img-fluid' style='width:22vh' src='" +
                            item.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                            "<td class='align-middle'>" + item.title + "</td>" +
                            "<td class='align-middle'>" + item.authors + "</td>" +
                            "<td class='align-middle'>" + item.publisher + "</td>" +
                            "<td class='align-middle'>" + item.publishedDate + "</td>" +
                            "<td class='align-middle'>"
                        }
                    $('#search-result').append(result);
            }
        });
    });
})
