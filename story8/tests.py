from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from .views import story8, fungsi_data

# Create your tests here.
class Story8UnitTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.response = self.client.get('/story8/')
        self.page_content = self.response.content.decode('utf8')

    def test_story8_url_exists(self):
        self.assertEqual(self.response.status_code, 200)

    def test_story8_check_template_used(self):
        self.assertTemplateUsed(self.response, 'story8.html')
    
    def test_story8_check_function_used(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, story8)

