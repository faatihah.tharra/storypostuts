from django.test import TestCase, Client
from django.http import HttpRequest

class UnitTest(TestCase):
    def test_landing_page_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'indexProfilku.html')

