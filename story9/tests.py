from django.test import TestCase
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.http import HttpRequest
import time
from .views import logIn, signUp, logOut
from .apps import Story9Config
import os

# Create your tests here.

class UnitTestonStory9(TestCase):

    #LOGIN PAGE TEST
    def test_does_login_page_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_check_function_used_by_login(self):
        found = resolve('/login/')
        self.assertEqual(found.func, logIn)

    def test_does_login_views_show_correct_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')
    
    def test_check_login_page_have_form(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form', html_response)
    
    def test_signin_header(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertContains(response, "Sign in")

    #SIGN UP PAGE TEST
    def test_does_signup_page_exist(self):
        url = reverse('signup')
        response = Client().get(url)
        self.assertEqual(response.status_code, 200)
    
    def test_check_function_used_by_login(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signUp)
    
    def test_signup_header(self):
        url = reverse('signup')
        response = Client().get(url)
        self.assertContains(response, "Create a new account")

    def test_does_signup_views_show_correct_template(self):
        url = reverse('signup')
        response = Client().get(url)
        self.assertTemplateUsed(response, 'signup.html')
    
    #SIGN OUT TEST
    def test_does_sign_out_work(self):
        url = reverse('logout')
        response = Client().get(url)
        self.assertEqual(response.status_code, 302)

class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(Story9Config.name, "story9")

