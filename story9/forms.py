from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm


class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'id' : 'signIn-username',
        'placeholder' : 'Username',
        'type' : 'text',
        'style' : 'width: 20vw;',
        'required' : True
            }
        )
    )

    password = forms.CharField(label="Password", widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'id' : 'signIn-password',
        'placeholder' : 'Password',
        'type' : 'password',
        'style' : 'width: 20vw;',
        'required' : True
            }
        )
    )

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(label="First name", widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'id' : 'signUp-firstName',
        'placeholder' : 'First name',
        'type' : 'text',
        'style' : 'width: 30vw;',
        'required' : True
            }
        )
    )

    last_name = forms.CharField(label="Last name", widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Last name',
        'id' : 'signUp-lastName',
        'type' : 'text',
        'style' : 'width: 30vw;',
        'required' : True
            }
        )
    )

    username = forms.CharField(label="Username", widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'id' : 'signUp-username',
        'placeholder' : 'Username',
        'type' : 'text',
        'style' : 'width: 30vw;',
        'required' : True
            }
        )
    )

    password1 = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={
        'class' : 'form-control',
        'id' : 'signUp-password1',
        'placeholder' : 'Password',
        'type' : 'password',
        'style' : 'width: 30vw;',
        'required' : True
            }
        )
    )
    
    password2 = forms.CharField(label="Confirm password", widget=forms.PasswordInput(attrs={
        'class' : 'form-control',
        'id' : 'signUp-password2',
        'placeholder' : 'Confirm Password',
        'type' : 'password',
        'style' : 'width: 30vw;',
        'required' : True
            }
        )  
    )

class Meta:
    model = User
    fields = ['first_name', 'last_name', 'username', 'password1', 'password2']

